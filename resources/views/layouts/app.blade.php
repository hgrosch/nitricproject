<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#1ea5d2">

    <title>Nitric</title>

    <link rel="icon" href="/favicon.ico" type="image/x-icon">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
    <![endif]-->

    <!--[if gte IE 9]<!-->
    <script src="//code.jquery.com/jquery-2.1.4.min.js"></script>
    <!--<![endif]-->

    <!-- Bootstrap -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">

    <link href="/css/app.css" rel="stylesheet" media="screen">

@yield('css')

<!-- Fonts -->
    <link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="{{ asset('pickadate/lib/themes/default.css') }}">
    <link rel="stylesheet" href="{{ asset('pickadate/lib/themes/default.date.css') }}">

    @yield('script')
</head>
<body>

<div class="container page">

    <header class="header">
        <div class="row">
            <div class="col-md-6">
                <a id="ccmc-logo" href="/">
                    <img src="/images/logo.jpg" alt="CCMC Logo" title="Connecticut Children's Medical Center" >
                </a>
            </div>
            <div id="site-name" class="col-md-6 text-right">
                <h1>Nitrous Oxide</h1>
            </div>
        </div>
    </header>

    <nav id="banner" class="navbar-default" role="navigation">
        <button class="navbar-toggle pull-left" type="button" data-toggle="collapse" data-target="#main-nav">
            <span class="sr-only">
                <img src="/images/icons/menu-xpand.png">
            </span>
            <span class="icon-bar"> </span>
            <span class="icon-bar"> </span>
            <span class="icon-bar"> </span>
        </button>
        <div class="collapse navbar-collapse" id="main-nav">
            <ul class="nav navbar-nav menu">

            </ul>
            <ul class="nav navbar-right">
                <li>
                    @if(Auth::user())
                        <a href="/logout">Log Out</a>
                    @else
                        <a href="/login">Log In</a>
                    @endif
                </li>
            </ul>
        </div>
    </nav>

    <section class="row page-content">
        <div class="col-md-12">
            @yield('content')
        </div>
    </section>

    <div class="row">
        <div class="col-md-12">
            <footer>
                <ul>
                    <li>&copy; Connecticut Children's Medical Center {{date('Y', time())}}</li>
                </ul>
            </footer>
        </div>
    </div>

</div>

<div class="bottom-spacer"></div>


</body>
</html>
