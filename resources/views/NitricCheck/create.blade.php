<!--
 * Created by PhpStorm.
 * User: hgrosch
 * Date: 5/1/2018
 * Time: 9:39 AM
 -->

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Nitric_Check for Connecticut Childrens Medical Center</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
    <script type="text/javascript" src="bootstrap-datepicker.de.js" charset="UTF-8"></script>


    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" />
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-theme.min.css" />
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />

    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script src="//use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>




</head>
<body>
<div id="main">
    <div class="container" >
        <h2>Nitrous Oxide Check</h2><br><br>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>/>
        @endif
        @if (\Session::has('success'))
            <div class="alert alert-success">
                <p>{{ \Session::get('success') }}</p>
            </div>
        @endif

        <div class="row">
            <div class="col-md-8">
                <form method="POST" action="{{'/NitricCheck'}}">
                    {{csrf_field()}}

                    <div class="row">
                        <div class="col-md-4">
                            <label>Shrink_Rap:</label>
                        </div>

                        <div class="col-md-8">
                            <input type="checkbox" name="shrink_rap">
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <label>Red_Pin_In_Place:</label>
                        </div>

                        <div class="col-md-8">
                            <input type="checkbox" name="Red_Pin_In_Place">
                        </div>

                    </div>

                    <div class="row">
                        <div class="form-group col-md-3">
                            <label>Tank_Number:</label>
                        </div>

                        <div class="col-md-9">
                            <input type="text" name="tank_number" value="">
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-3">
                            <label>USB_HRS:</label>
                        </div>

                        <div class="col-md-9">
                            <input type="text" name="usb_hrs">
                        </div>
                    </div>

                <!--DatePicker -->
                    <div class="row">
                        <div class="form-group col-md-3">
                            <label>Arrival Date</label>
                        </div>

                        <div class="container col-md-9">
                            <!--<input class="date form-control" type="text">-->
                            <input class="date datepicker" name="tank_arrival_date">
                        </div>
                    </div>

                    <script type="text/javascript">
                        $('.date').datepicker({
                            autoclose: true,
                            format: 'yyyy-mm-dd'
                        });
                    </script><br>


                    <!--DatePicker -->
                    <div class="row">
                        <div class="form-group col-md-3">
                            <label>Start Date</label>
                        </div>

                        <div class="container col-md-9">
                            <!--<input class="date form-control" type="text">-->
                            <input class="date datepicker" name="start_date">
                        </div>
                    </div>

                    <script type="text/javascript">
                        $('.date').datepicker({
                            autoclose: true,
                            format: 'yyyy-mm-dd'
                        });
                    </script><br>
                    <!-- hans  -->

                    <div class="row">
                        <div class="form-group col-md-3">
                            <label>Start_Time:</label>
                        </div>

                        <div class="col-md-9">
                            <input type="time" name="start_time" value="">
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-3">
                            <label>UserName:</label>
                        </div>

                        <div class="col-md-9">
                            <input type="text" name="samaccountname" value="{{ Auth::user()->name }}" disabled>
                        </div>
                    </div>

                    <div>
                        <div class="row">
                            <div class="col-md-3">
                                <label>Status:</label>
                            </div>

                            <div class="col-md-9">
                                <select name="status" autofocus>

                                    <option>Select Status.</option>

                                    @foreach($status as $value)
                                        <option value="{{$value->value}}">{{$value->value}}</option>
                                    @endforeach

                                </select>
                            </div>
                        </div>
                    </div><br>


                    <div class="row">
                        <div class="form-group col-md-3">
                            <label>Medical_Record:</label>
                        </div>

                        <div class="col-md-9">
                            <input type="text" name="Medical_Record">
                        </div>
                    </div>
                    <br>


                    <div class="row">
                        <div class="col-md-4"></div>
                        <div class="form-group col-md-8">
                            <button type="submit" class="btn btn-success" style="margin-center:1px">Submit</button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
        <br>
        <div class="col-md-2">
            <img class="right snapshot" src="/images/nitricimage.jpg" width="500" height="300">
        </div>
    </div>


</div>

</body>

</html>
