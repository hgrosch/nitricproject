<!--
 * Created by PhpStorm.
 * User: hgrosch
 * Date: 5/1/2018
 * Time: 9:39 AM
 -->

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Nitrous Oxide</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">


</head>
<body>
<div id="main">
    <div class="container">
        <h2>Checked Tanks</h2>

        <div class="jumbotron text-center">
            <h2></h2>

            @foreach($checks as $check)
                <div class="row">

                    <div class="col-sm-1 col-sm-push-4" style="background-color:lightyellow;">
                        {{ $check->tank_number }}
                    </div>

                    <div class="col-sm-2 col-sm-push-4" style="background-color:lavenderblush;">
                        {{ $check->usb_hrs }}
                    </div>

                    <div class="col-sm-2 col-sm-push-4" style="background-color:lavenderblush;">
                        {{ $check->status }}
                    </div>

                    <div class="col-sm-2 col-sm-push-4" style="background-color:lavenderblush;">
                        {{  $check->samaccountname }}
                    </div>

                    <div class="col-sm-2 col-sm-push-4" style="background-color:lightcyan;">
                        {{ $check->created_at }}
                    </div>

                    <div class="col-sm-1 col-sm-push-4" style="background-color:lightyellow;">
                        {{ $check->nitric_id }}
                    </div>

                </div>

            @endforeach
        </div>

        <div class="row">

            <div class="form-group col-md-10 col-lg-offset-2 text-center">
                <!--<a class="btn btn-primary" href="http://webtstrch01t:8880/CodeCarts/show" role="button">Home</a>-->
                <a class="btn btn-primary" href="{{ route('login') }}" role="button">Home</a>
            </div><br><br><br>

        </div>


    </div>
</div>
<script src="/js/app.js"></script>
</body>

</html>