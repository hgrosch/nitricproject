<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NitricCheck extends Model
{
protected $table = 'nitric_checks';

protected $fillable = ['nitric_id','shrink_rap','red_pin_in_place','tank_number','usb_hrs','tank_arrival_date','start_time','start_date','samaccountname','status','Medical_Record'];
}

