<?php

namespace App\Http\Controllers;

use App\NitricCheck;
use App\status;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NitricCheckController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $status = status::all('id','value');
        $nitric_checks = nitriccheck::all('nitric_id','shrink_rap','red_pin_in_place','tank_number','usb_hrs','tank_arrival_date','start_time','start_date','Medical_Record','Visit_Number','samaccountname','status');

        return view('NitricCheck.create', compact('status','nitric_checks'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,
            [
                'tank_number' =>'required',
                'status' => 'required'
               // 'nitric_id' => 'required'
            ]);

        nitriccheck::create([
            'nitric_id' =>$request->get('nitric_id'),
            'shrink_rap'=>$request->has('shrink_rap'),
            'red_pin_in_place'=>$request->has('red_pin_in_place'),
            'tank_number'=>$request->get('tank_number'),
            'usb_hrs'=>$request->get('usb_hrs'),
            'tank_arrival_date'=>$request->get('tank_arrival_date'),
            'start_time'=>$request->get('start_time'),
            'start_date'=>$request->get('start_date'),
            'samaccountname'=> Auth::user()->samaccountname,
            'status'=>$request->get('status'),
            'Medical_Record'=>$request->get('Medical_Record'),
            'Visit_number'=>$request->get('Visit_Number')


        ]);

        return back()->with('success', 'Nitric Tank has been checked');
        //return response()->redirectToRoute('NitricCheck.show')->with('success', 'Nitric Tank has been checked.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\NitricCheck  $nitricCheck
     * @return \Illuminate\Http\Response
     */
    public function show(NitricCheck $nitricCheck)
    {
       $checks = NitricCheck::all();

       return view('NitricCheck.show', compact('checks','tank_number','usb_hrs','status','samaccountname','Medical_Record','created_at','nitric_id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\NitricCheck  $nitricCheck
     * @return \Illuminate\Http\Response
     */
    public function edit(NitricCheck $nitricCheck)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\NitricCheck  $nitricCheck
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, NitricCheck $nitricCheck)
    {
        //Auth::user()
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\NitricCheck  $nitricCheck
     * @return \Illuminate\Http\Response
     */
    public function destroy(NitricCheck $nitricCheck)
    {
        //
    }
}
