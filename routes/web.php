<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
   // return view('welcome');


//});

Route::get('/', 'Auth\LoginController@showLoginForm');

//Auth::routes();

Route::get('login', ['as' => 'login', 'uses' => 'Auth\LoginController@showLoginForm']);
Route::post('login', ['as' => 'login', 'uses' => 'Auth\LoginController@login']);
Route::get('logout', ['as' => 'logout', 'uses' => 'Auth\LoginController@logout']);
//Route::get('NitricCheck/create', 'NitricCheckController@show');
//Route::get('/home', 'HomeController@index')->name('home');


Route::resource('NitricCheck','NitricCheckController');
Route::get('NitricCheck/show','NitricCheckController@show');
Route::get('NitricCheck/show','NitricCheckController@show')->name('NitricCheck.list');
