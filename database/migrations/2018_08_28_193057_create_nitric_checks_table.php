<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNitricChecksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nitric_checks', function (Blueprint $table)
        {
            $table->increments('nitric_id');
            $table->boolean('shrink_rap');
            $table->boolean('red_pin_in_place');
            $table->string('tank_number');
            $table->string('usb_hrs');
            $table->date('tank_arrival_date');
            $table->time('start_time');
            $table->date('start_date');
            $table->string('samaccountname');
            $table->string('status');
            $table->string('Medical_Record');
            $table->string('Visit_Number');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nitric_checks');
    }
}
